package com.avocatapp;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.*;

/**
 * Created by Lucian on 11/19/2015.
 */
public class ClientView extends CustomComponent implements View,
        Button.ClickListener {


    public static final String NAME = "client";

    private final TextField name;
    private final TextField email;
    private final TextField telefon;
    private final DateField createDate;
    private final DateField closeDate;
    private final Button addClient;


public ClientView(){
    setSizeFull();

    VerticalLayout main = new VerticalLayout();
    main.setSizeFull();
    name = new TextField("Nume");
    email = new TextField("Nume");
    telefon = new TextField("Nume");
    createDate = new DateField("Data adaugare");
    closeDate = new DateField("Data inchidere");
    addClient = new Button("Adauga client", this);
    main.addComponents(name, email, telefon, createDate, closeDate);
    main.setComponentAlignment(name, Alignment.MIDDLE_CENTER);
    main.setComponentAlignment(email, Alignment.MIDDLE_CENTER);
    main.setComponentAlignment(telefon, Alignment.MIDDLE_CENTER);
    main.setComponentAlignment(createDate, Alignment.MIDDLE_CENTER);
    main.setComponentAlignment(closeDate, Alignment.MIDDLE_CENTER);
    main.setCaption("Clienti");
    main.setSpacing(true);
    main.setMargin(new MarginInfo(true, true, true, true));
//        borderLayout.setComponentAlignment(fields,BorderLayout.A.MIDDLE_CENTER);

    // The view root layout
//    VerticalLayout viewLayout = new VerticalLayout(borderLayout);
//    viewLayout.setSizeFull();
//    viewLayout.setStyleName(Reindeer.LAYOUT_BLUE);
    setCompositionRoot(main);
}

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        // focus the username field when user arrives to the login view
        name.focus();
    }

    @Override
    public void buttonClick(Button.ClickEvent clickEvent) {
        name.setValue("ADAUGA");

    }
}
