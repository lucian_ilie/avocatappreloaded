package com.avocatapp.util;

import org.vaadin.appfoundation.authentication.util.PasswordUtil;
import org.vaadin.appfoundation.authentication.util.UserUtil;
import org.vaadin.appfoundation.persistence.facade.FacadeFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.Properties;

/**
 * Created by Lucian on 11/18/2015.
 */
@WebListener( "Context listener for doing something or other." )
public class MyServletContextListener implements  ServletContextListener {




        // Vaadin app deploying/launching.
        @Override
        public void contextInitialized ( ServletContextEvent contextEvent )
        {

            try {
                FacadeFactory.registerFacade("mysql", true);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            // Set the salt for passwords
            Properties prop = new Properties();
            prop.setProperty("password.salt", "pfew4‰‰#fawef@53424fsd");
            PasswordUtil.setProperties(prop);

            // Set the properties for the UserUtil
            prop.setProperty("password.length.min", "4");
            prop.setProperty("username.length.min", "4");

            UserUtil.setProperties(prop);
            ServletContext context = contextEvent.getServletContext();
            // …
        }

        // Vaadin app un-deploying/shutting down.
        @Override
        public void contextDestroyed ( ServletContextEvent contextEvent )
        {
            ServletContext context = contextEvent.getServletContext();
            // …
        }


}
