package com.avocatapp.pojo;

import org.vaadin.appfoundation.persistence.data.AbstractPojo;

import javax.persistence.*;
import java.util.Date;

/**
* Created by Lucian on 11/17/2015.
*/
@Entity
public class Termen extends AbstractPojo {

    private static final long serialVersionUID = -21L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String numar;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data", nullable = false)
    private Date data;
//    @ManyToOne
//    private Dosar dosar;
    private Solutie solutie;
    private String notes;


    @Column(nullable = false)
    @Version
    protected Long consistencyVersion;


    public Termen(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumar() {
        return numar;
    }

    public void setNumar(String numar) {
        this.numar = numar;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

//    public Dosar getDosar() {
//        return dosar;
//    }
//
//    public void setDosar(Dosar dosar) {
//        this.dosar = dosar;
//    }

    public Solutie getSolutie() {
        return solutie;
    }

    public void setSolutie(Solutie solutie) {
        this.solutie = solutie;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
