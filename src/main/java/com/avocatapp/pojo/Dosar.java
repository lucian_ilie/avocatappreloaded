package com.avocatapp.pojo;

import org.vaadin.appfoundation.persistence.data.AbstractPojo;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Lucian on 11/17/2015.
 */
@Entity
public class Dosar extends AbstractPojo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    private String numar;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createDate", nullable = true)
    private Date createDate;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "closeDate", nullable = true)
    private Date closeDate;

    private String notes;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "lastUpdated", nullable = true)
    private Date lastUpdated;
    @ManyToOne
    private Client client;


//    @OneToMany(mappedBy = "dosar")
//    private List<Termen> termene;


    @Column(nullable = false)
    @Version
    protected Long consistencyVersion;


    public Dosar(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumar() {
        return numar;
    }

    public void setNumar(String numar) {
        this.numar = numar;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

//    public List<Termen> getTermene() {
//        return termene;
//    }
//
//    public void set
// Termene(List<Termen> termene) {
//        this.termene = termene;
//    }
}
