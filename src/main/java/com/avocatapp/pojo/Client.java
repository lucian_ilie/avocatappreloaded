package com.avocatapp.pojo;

import org.vaadin.appfoundation.persistence.data.AbstractPojo;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Lucian on 11/17/2015.
 */
@Entity
public class Client extends AbstractPojo  {

    private static final long serialVersionUID = -7289994339186082141L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "nume", nullable = false)
    private String nume;
    @Column(name = "adresa", nullable = true)
    private String adresa;
    @Column(name = "email", nullable = true)
    private String email;
    @Column(name = "telefon", nullable = true)
    private String telefon;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createDate", nullable = false)
    private Date createDate;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "closeDate", nullable = true)
    private Date closeDate;
    private String notes;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "lastUpdated", nullable = true)
    private Date lastUpdated;
    @OneToMany(mappedBy = "client")
    private List<Dosar> dosare;


    @Column(nullable = false)
    @Version
    protected Long consistencyVersion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public List<Dosar> getDosare() {
        return dosare;
    }

    public void setDosare(List<Dosar> dosare) {
        this.dosare = dosare;
    }

    public Client() {
    }
}
