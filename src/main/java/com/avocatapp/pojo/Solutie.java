package com.avocatapp.pojo;

import org.vaadin.appfoundation.persistence.data.AbstractPojo;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Lucian on 11/17/2015.
 */
@Entity
public class Solutie extends AbstractPojo {

    private static final long serialVersionUID =  243232534534534345L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String numar;
    private String solutie;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data", nullable = false)
    private Date data;
    private Termen termen;

    @Column(nullable = false)
    @Version
    protected Long consistencyVersion;

    public Solutie(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumar() {
        return numar;
    }

    public void setNumar(String numar) {
        this.numar = numar;
    }

    public String getSolutie() {
        return solutie;
    }

    public void setSolutie(String solutie) {
        this.solutie = solutie;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Termen getTermen() {
        return termen;
    }

    public void setTermen(Termen termen) {
        this.termen = termen;
    }
}
