package com.avocatapp;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.*;
import org.vaadin.addon.borderlayout.BorderLayout;
//import com.vaadin.ui.CustomComponent;
//import com.vaadin.ui.Label;

/**
 * Created by Lucian on 11/17/2015.
 */
public class MainView extends CustomComponent implements View {

    public static final String NAME = "";

    Label text = new Label();
    private Button clientsButton;
    private Button dosareButton;
    private Button termeneButton;

    private BorderLayout borderLayout;

    Button logout = new Button("Logout", new Button.ClickListener() {

        @Override
        public void buttonClick(Button.ClickEvent event) {

            // "Logout" the user
            getSession().setAttribute("user", null);

            // Refresh this view, should redirect to login view
            getUI().getNavigator().navigateTo(NAME);
        }
    });

    public MainView() {

        borderLayout = new BorderLayout();
        borderLayout.setSizeFull();


        //layout header
        HorizontalLayout headerLayout = new HorizontalLayout();
        headerLayout.setSizeFull();
        headerLayout.setSpacing(true);
        headerLayout.setHeight("100px");
        headerLayout.addComponent(text);
        headerLayout.addComponent(logout);
        headerLayout.setComponentAlignment(text, Alignment.TOP_RIGHT);
        headerLayout.setComponentAlignment(logout,Alignment.BOTTOM_RIGHT);
        headerLayout.setStyleName("header");


        //left panel layout
        VerticalLayout leftLayout = new VerticalLayout();
        leftLayout.setSizeFull();
        leftLayout.setSpacing(true);
        leftLayout.setWidth("200px");
         clientsButton = new Button("Clienti");
         dosareButton = new Button("Dosare");
         termeneButton = new Button("Termene");
        addButtonsListeners();

        clientsButton.setWidth("200px");
        dosareButton.setWidth("200px");
        termeneButton.setWidth("200px");


        leftLayout.addComponents(clientsButton,dosareButton,termeneButton);
        //footer header
        HorizontalLayout footerLayout = new HorizontalLayout();
        footerLayout.setSizeFull();
        footerLayout.setHeight("100px");
        footerLayout.setStyleName("header");


        //add header layout to main borderlayout
        borderLayout.addComponent(footerLayout, BorderLayout.Constraint.SOUTH);
        borderLayout.addComponent(leftLayout, BorderLayout.Constraint.WEST);
        borderLayout.addComponent(headerLayout, BorderLayout.Constraint.NORTH);
        setCompositionRoot(borderLayout);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        // Get the user name from the session
        String username = String.valueOf(getSession().getAttribute("user"));

        // And show the username
        text.setValue("Hello " + username);
    }

    private void addButtonsListeners(){

//        this.getClientsButton().addClickListener(new Button.ClickListener() {
//            public void buttonClick(Button.ClickEvent event) {
//                Client newClient = new Client();
//                newClient.setNume("Test Client");
//                newClient.setAdresa("Test Client Adresa");
//                newClient.setCreateDate(new Date());
//                List<Dosar> listDosare = new ArrayList<Dosar>();
//                Dosar dosar1 = new Dosar ();
//                dosar1.setNumar("12131233");
//                FacadeFactory.getFacade().store(dosar1);
//                listDosare.add(dosar1);
//                newClient.setDosare(listDosare);
//                FacadeFactory.getFacade().store(newClient);
//                clientsButton.setCaption("Client was saved!");
//            }
//        });

        this.getClientsButton().addClickListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                ClientView clientView = new ClientView();
                borderLayout.addComponent(clientView,BorderLayout.Constraint.CENTER);
                clientView.setSizeFull();
            }
        });
    }

    public Button getLogout() {
        return logout;
    }

    public void setLogout(Button logout) {
        this.logout = logout;
    }

    public Button getClientsButton() {
        return clientsButton;
    }

    public void setClientsButton(Button clientsButton) {
        this.clientsButton = clientsButton;
    }

    public Button getDosareButton() {
        return dosareButton;
    }

    public void setDosareButton(Button dosareButton) {
        this.dosareButton = dosareButton;
    }

    public Button getTermeneButton() {
        return termeneButton;
    }

    public void setTermeneButton(Button termeneButton) {
        this.termeneButton = termeneButton;
    }

    public BorderLayout getBorderLayout() {
        return borderLayout;
    }

    public void setBorderLayout(BorderLayout borderLayout) {
        this.borderLayout = borderLayout;
    }

    public Label getText() {
        return text;
    }

    public void setText(Label text) {
        this.text = text;
    }
}